import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { LogService } from './common/logger/log.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule , {
    logger: new LogService()
  });
  await app.listen(3000);
}
bootstrap();
