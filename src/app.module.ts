import { Module, MiddlewareConsumer } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SourceOfCareModule } from './app/source-of-care/source-of-care.module';
import { LoggerMiddleware } from './common/logger/logging.middleware';
import { AuthorizationMiddleware } from './common/authorization/authorization.middleware';

@Module({
  imports: [SourceOfCareModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(LoggerMiddleware, AuthorizationMiddleware)
      .forRoutes('*');
  }
}
