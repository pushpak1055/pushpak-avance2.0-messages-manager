import { Controller, Request, UseFilters, Get, Param } from '@nestjs/common';
import { SourceOfCareService } from './source-of-care.service';
import { HttpExceptionFilter } from 'src/common/exception/http-exception.filter';

@Controller('v1/patient-triaging/source-of-care')
export class SourceOfCareController {
    constructor(private readonly sourceOfCareService: SourceOfCareService) {}

    @Get(':id')
    @UseFilters(new HttpExceptionFilter())
    async findAll(@Request() req, @Param('id') id): Promise<string>{
        return this.sourceOfCareService.sourceOfCare(req.query, req.options.auth, id);   
    }
}
