import { Injectable, HttpException, LoggerService } from '@nestjs/common';
import { ConfigService } from 'src/common/configuration/config.service';
import Axios from 'axios';
import { ErrorService  } from 'src/common/error-handler/error.service';


@Injectable()
export class SourceOfCareService {

    private sandboxUrl: string;
    constructor(config: ConfigService,
        private readonly error: ErrorService
        ){
        this.sandboxUrl = config.get('SANDBOX_URL')+config.get('SOURCE_OF_CARE');
    }

    async sourceOfCare(query: any, auth: any, id :number): Promise<string> {
        let response = null;
        try{
            response = await Axios({
                method: 'GET',
                url: this.sandboxUrl+'/'+id,
                params: query,
                auth: auth
                });
        }catch(e){
            this.error.handle(e);
        }
        return response.data;
    }
}
