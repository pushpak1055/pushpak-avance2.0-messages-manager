import { Module } from '@nestjs/common';
import { SourceOfCareController } from './source-of-care.controller';
import { SourceOfCareService } from './source-of-care.service';
import { ConfigModule } from 'src/common/configuration/config.module';
import { ErrorModule } from 'src/common/error-handler/error.module';

@Module({ imports: [ConfigModule, ErrorModule],
    controllers: [SourceOfCareController],
    providers: [SourceOfCareService]})
export class SourceOfCareModule {}
