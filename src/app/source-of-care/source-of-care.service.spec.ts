import { Test, TestingModule } from '@nestjs/testing';
import { SourceOfCareService } from './source-of-care.service';

describe('SourceOfCareService', () => {
  let service: SourceOfCareService;
  
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SourceOfCareService],
    }).compile();
    service = module.get<SourceOfCareService>(SourceOfCareService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
