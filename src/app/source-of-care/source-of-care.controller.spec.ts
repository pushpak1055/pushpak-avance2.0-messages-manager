import { Test, TestingModule } from '@nestjs/testing';
import { SourceOfCareController } from './source-of-care.controller';

describe('SourceOfCare Controller', () => {
  let module: TestingModule;
  
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [SourceOfCareController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: SourceOfCareController = module.get<SourceOfCareController>(SourceOfCareController);
    expect(controller).toBeDefined();
  });
});
