import { Injectable, NestMiddleware, MiddlewareFunction, Inject } from '@nestjs/common';
import {ConfigService} from '../configuration/config.service';

@Injectable()
export class AuthorizationMiddleware implements NestMiddleware {
    private user:string;
    private password:string;
    constructor(config: ConfigService){
        this.user = config.get('AUTHORIZATION.USER');
        this.password = config.get('AUTHORIZATION.PASSWORD');
    }

  resolve(...args: any[]): MiddlewareFunction {
    return (req, res, next) => {
        req.options = {
            json: true,
            auth: {
              username: this.user,
              password: this.password
            }
          };
          if (req.method === "POST" || req.method === "PUT") {
            req.options.headers = {
              "Content-Type": "application/json"
            };
          }
      next();
    };
  }
}