import { Module } from '@nestjs/common';
import { LogService } from './log.service';
import { ConfigModule } from '../configuration/config.module';

@Module({
    imports: [ConfigModule],
    providers: [LogService],
    exports: [LogService]
})
export class LogModule {}
