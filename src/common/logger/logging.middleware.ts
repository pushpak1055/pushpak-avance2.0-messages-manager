import { Injectable, NestMiddleware, MiddlewareFunction, LoggerService } from '@nestjs/common';
import { LogService } from './log.service';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {

  constructor(private readonly log: LogService) {}

  resolve(...args: any[]): MiddlewareFunction {
    return (req, res, next) => {
      let start_time: number = new Date().getTime();
      let baseUrl = req.baseUrl
      next();
      this.log.log(`Total Time taken for the request to  ${baseUrl} is ${(new Date().getTime() - start_time)} ms`);
    };
  }
}