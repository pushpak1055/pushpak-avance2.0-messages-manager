import { LoggerService, Inject } from '@nestjs/common';
import * as winston from  'winston';
import  * as DailyRotateFile from 'winston-daily-rotate-file';
import { ConfigService } from '../configuration/config.service';

const configg = winston.config;
const timeStampFormat = "YYYY-MM-DDTHH:mm:ss.SSSZ";
const moment = require("moment");
const loggingInfo =  {
    logging :
        {
        "path": "./logs/ACH-%DATE%.log",
        "enableLogging": true,
        "console": false,
        "debugLevel":true,
        "infoLevel":false,
        "errorLevel":false
        }
};

const LOG_PATH = loggingInfo.logging.path;
const silent = loggingInfo.logging.enableLogging ? false : true;
const DEBUG_LEVEL = loggingInfo.logging.debugLevel;
const INFO_LEVEL = loggingInfo.logging.infoLevel;
const ERROR_LEVEL = loggingInfo.logging.errorLevel;
var LOG_LEVEL;

if (DEBUG_LEVEL == true && INFO_LEVEL == false && ERROR_LEVEL == false) {
  LOG_LEVEL = "debug";
} else if (DEBUG_LEVEL == false && INFO_LEVEL == true && ERROR_LEVEL == false) {
  LOG_LEVEL = "info";
} else if (DEBUG_LEVEL == false && INFO_LEVEL == false && ERROR_LEVEL == true) {
  LOG_LEVEL = "error";
} else {
  LOG_LEVEL = "debug";
}

const opt = {
    name: "DailyRotateFile",
    filename: LOG_PATH,
    datePattern: "YYYY-MM-DD",
    zippedArchive: true,
    level: LOG_LEVEL,
    maxSize: "20m",
    maxFiles: "7d",
    silent: silent,
    // handleExceptions: true,
    timestamp: function() {
      return moment().format(timeStampFormat);
    },
    formatter: function(options) {
      return (
        options.timestamp() +
        " " +
        options.level.toUpperCase() +
        " " +
        " " +
        (options.message ? options.message : "") +
        ", PID: " +
        process.pid +
        (options.meta && Object.keys(options.meta).length
          ? "\n\t" + JSON.stringify(options.meta)
          : "")
      );
    }
  };

  const logger = winston.createLogger({
    transports: [
      new winston.transports.Console({ format: winston.format.simple()}),
      new DailyRotateFile(opt)
    ]
  });
  
  if (!loggingInfo.logging.console) {
    logger.remove(new winston.transports.Console());
  }
  
  if (silent) {
    logger.remove(DailyRotateFile);
  }

export class LogService implements LoggerService {
  log(message: string) {
      logger.log({
          level:'debug',
          message: message
      });
  }
  error(message: string, trace: string) {
    logger.log({
        level:'info',
        message: message
    });
    logger.log({
        level:'error',
        message: trace
    });
  }
  warn(message: string) {
    logger.log({
        level:'warn',
        message: message
    });
  }
}