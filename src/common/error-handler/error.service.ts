import { HttpException } from '@nestjs/common';
import { LogService } from '../logger/log.service';

export class ErrorService {
  
  constructor(private log: LogService) {}

  handle(e: any): void {
    let errorCode: number = 500;
    let errorInfo =  'Health Navigator Cannot be reached';
    if(e.response){
        errorCode = e.response.status;
        errorInfo = e.response.data[0];
    }
    this.log.error(errorInfo, e);
    throw new HttpException(errorInfo, errorCode);
  }
}