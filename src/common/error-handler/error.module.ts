import { Module } from '@nestjs/common';
import { ErrorService } from './error.service';
import { LogModule } from '../logger/log.module';
import { ConfigModule } from '../configuration/config.module';

@Module({
    imports: [LogModule],
    providers: [ErrorService],
    exports: [ErrorService]
})
export class ErrorModule {}
